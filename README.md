# React Playground, brought to you by: The Captain
The short install relies upon the package.json... The long install demonstrates how you would set up REACT on a new project.

## Installation Instructions (Short Install):
This installations assumes you have node installed... Issue the following commands via terminal from the project root.

* npm install (make sure your permissions are correct, should be a clean install)


## Installation Instructions (Long Install):
This installations assumes you have node installed... Issue the following commands, via terminal, from the project root (one by one in case something fails).

* npm install --save-dev vinyl-source-stream
* npm install --save-dev browserify
* npm install --save-dev gulp
* npm install --save-dev watchify
* npm install --save-dev reactify
* npm install --save-dev react
* npm install --save-dev react-dom



## Start Gulp:
Via terminal, navigate to the "_js" dir and issue the command:

* gulp

Gulp is now running, because there is a file watcher working with gulp all you have to do is modify: main.js.... Gulp will update app.js for you.. After you make your changes, refresh your browser.


