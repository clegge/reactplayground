var React = require('react');
var ReactDOM = require('react-dom');
var ProductsList = require('./react/ProductsList.react.js')
var AppHeader = require('./react/AppHeader.react.js')

// Could come from an API, LocalStorage, another component, etc...
var products = [
    {brand: 'Wonder', name: 'Bread', inStock: 'Yes', price: 1.99},
    {brand: 'Truetein', name: 'Whey', inStock: 'Yes', price: 39.99},
    {brand: 'Hormel', name: 'Bacon', inStock: 'No',price: 3.49},
    {brand: 'Jens Farms', name: 'Eggs',  inStock: 'Yes',price: 2.29},
    {brand: 'Sheetz', name: 'Coffee', inStock: 'Yes',price: .95}
];

var Main = React.createClass({
    render: function () {
        return (
            <div className="row">
                <div className="col-xs-12">
                    <AppHeader/>
                    <ProductsList products={products}/>
                </div>
            </div>
        );
    }
})

ReactDOM.render(<Main/>, document.getElementById('react-application'));

