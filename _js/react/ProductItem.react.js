var React = require('react');

var ProductItem = React.createClass({
    render: function () {
        return (
            <tr>
                <td>{this.props.name}</td>
                <td>{this.props.brand}</td>
                <td>{this.props.price}</td>
                <td>{this.props.inStock}</td>
            </tr>
        );
    }
})

module.exports = ProductItem;