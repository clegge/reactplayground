var React = require('react');
var ProductItem = require('./ProductItem.react.js')

var ProductsList = React.createClass({
    render: function () {
        var products = this.props.products.map(function (product, index) {
            return (
                <ProductItem
                    key={index}
                    brand={product.brand}
                    name={product.name}
                    inStock={product.inStock}
                    price={product.price}/>
            );
        });

        return (

            <table className="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>ITEM</th>
                    <th>BRAND</th>
                    <th>PRICE</th>
                    <th>AVAILABLE</th>
                </tr>
                </thead>
                <tbody>
                {products}
                </tbody>
            </table>
        );
    }
})

module.exports = ProductsList;